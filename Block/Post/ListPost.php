<?php

namespace Dockie\Blog\Block\Post;

use Dockie\Blog\Api\Data\PostInterface;
use Dockie\Blog\Api\PostRepositoryInterface;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class ListPost extends Template
{

    /**
     * @var Context
     */
    protected $context;

    /**
     * @var array
     */
    protected $data;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var PostRepositoryInterface
     */
    protected $postRepository;

    /**
     * @param Context $context
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param PostRepositoryInterface $postRepository
     * @param array $data
     */
    public function __construct(
        Context $context,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        PostRepositoryInterface $postRepository,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->postRepository = $postRepository;
    }


    /**
     * @return PostInterface[]|null
     */
    public function getBlogPosts(){
        $criteria = $this->searchCriteriaBuilder->create();
        $result = $this->postRepository->getList($criteria);
        if (!$result->getTotalCount()) {
            return null;
        }

        $items = $result->getItems();
        return $items;
    }
}