<?php
/**
 * NOTICE OF LICENSE
 * This source file is subject to the General Public License (GPL 3.0).
 * This license is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/gpl-3.0.en.php
 *  
 * DISCLAIMER
 * Do not edit or add to this file if you wish to upgrade this module to newer
 * versions in the future.
 * @category    Dockie: Blog package
 * @package     Dockie: Blog
 * @author        Willem Oostelbos <willemoostelbos@gmail.com>
 * @copyright   Copyright (c) 2019 Willem Oostelbos (https://www.willemoostelbos.nl)
 * @license        http://opensource.org/licenses/gpl-3.0.en.php General Public
 * License (GPL 3.0)
 */

namespace Dockie\Blog\Model\ResourceModel;

use Dockie\Blog\Api\Data\PostInterface;
use Magento\Framework\EntityManager\EntityManager;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

class Post extends AbstractDb
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @param Context $context
     * @param EntityManager $entityManager
     * @param null $connectionName
     */
    public function __construct(
        Context $context,
        EntityManager $entityManager,
        $connectionName = null
    ) {
        parent::__construct($context, $connectionName);
        $this->entityManager = $entityManager;
    }

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(
            PostInterface::TABLE,
            PostInterface::KEY_POST_ID
        );
    }

    /**
     * @inheritDoc
     */
    public function load(
        AbstractModel $object,
        $value,
        $field = null
    ) {
        $this->entityManager->load($object, $value);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function save(AbstractModel $object)
    {
        $this->entityManager->save($object);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function delete(AbstractModel $object)
    {
        $this->entityManager->delete($object);

        return $this;
    }
}