<?php
/**
 * NOTICE OF LICENSE
 * This source file is subject to the General Public License (GPL 3.0).
 * This license is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/gpl-3.0.en.php
 *
 * DISCLAIMER
 * Do not edit or add to this file if you wish to upgrade this module to newer
 * versions in the future.
 * @category    Dockie: Blog package
 * @package     Dockie: Blog
 * @author        Willem Oostelbos <willemoostelbos@gmail.com>
 * @copyright   Copyright (c) 2019 Willem Oostelbos (https://www.willemoostelbos.nl)
 * @license        http://opensource.org/licenses/gpl-3.0.en.php General Public
 * License (GPL 3.0)
 */

namespace Dockie\Blog\Model;

use Dockie\Blog\Api\Data\PostInterface;
use Dockie\Blog\Model\ResourceModel\Post as PostResource;
use Magento\Framework\Model\AbstractModel;

class Post extends AbstractModel implements PostInterface
{
    protected function _construct()
    {
        $this->_init(PostResource::class);
    }

    /**
     * @inheritDoc
     */
    public function getPostId(){
        return $this->getId();
    }

    /**
     * @inheritDoc
     */
    public function setPostId($postId){
        return $this->setId($postId);
    }

    /**
     * @inheritDoc
     */
    public function getTitle()
    {
        return $this->getData(self::KEY_TITLE);
    }

    /**
     * @inheritDoc
     */
    public function setTitle($title)
    {
        return $this->setData(self::KEY_TITLE, $title);
    }

    /**
     * @inheritDoc
     */
    public function getAlias()
    {
        return $this->getData(self::KEY_ALIAS);
    }

    /**
     * @inheritDoc
     */
    public function setAlias($alias)
    {
        return $this->setData(self::KEY_ALIAS, $alias);
    }

    /**
     * @inheritDoc
     */
    public function getStatus()
    {
        return $this->getData(self::KEY_STATUS);
    }

    /**
     * @inheritDoc
     */
    public function setStatus($status)
    {
        return $this->setData(self::KEY_STATUS, $status);
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt()
    {
        return $this->getData(self::KEY_CREATED_AT);
    }

    /**
     * @inheritDoc
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::KEY_CREATED_AT, $createdAt);
    }

    /**
     * @inheritDoc
     */
    public function getUpdatedAt(){
        return $this->getData(self::KEY_UPDATED_AT);
    }

    /**
     * @inheritDoc
     */
    public function setUpdatedAt($updatedAt){
        return $this->setData(self::KEY_UPDATED_AT, $updatedAt);
    }

    /**
     * @inheritDoc
     */
    public function getShortDescription(){
        return $this->getData(self::KEY_SHORT_DESCRIPTION);
    }

    /**
     * @inheritDoc
     */
    public function setShortDescription($shortDescription){
        return $this->setData(self::KEY_SHORT_DESCRIPTION,$shortDescription);
    }

    /**
     * @inheritDoc
     */
    public function getDescription(){
        return $this->getData(self::KEY_DESCRIPTION);
    }

    /**
     * @inheritDoc
     */
    public function setDescription($description){
        return $this->setData(self::KEY_DESCRIPTION,$description);
    }
}