<?php

namespace Dockie\Blog\Model;

use Dockie\Blog\Api\Data\PostSearchResultsInterface;
use Dockie\Blog\Api\Data\PostInterface;
use Dockie\Blog\Api\PostRepositoryInterface;
use Dockie\Blog\Api\Data\PostInterfaceFactory;
use Dockie\Blog\Api\Data\PostSearchResultsInterfaceFactory;
use Dockie\Blog\Model\ResourceModel\Post as PostResource;
use Dockie\Blog\Model\ResourceModel\Post\Collection;
use Dockie\Blog\Model\ResourceModel\Post\CollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class PostRepository implements PostRepositoryInterface
{

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * @var PostSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var PostInterfaceFactory
     */
    protected $postFactory;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var PostResource
     */
    protected $postResource;

    /**
     * @param CollectionProcessorInterface $collectionProcessor
     * @param PostSearchResultsInterfaceFactory $searchResultsFactory
     * @param PostInterfaceFactory $postFactory
     * @param CollectionFactory $collectionFactory
     * @param PostResource $postResource
     */
    public function __construct(
        CollectionProcessorInterface $collectionProcessor,
        PostSearchResultsInterfaceFactory $searchResultsFactory,
        PostInterfaceFactory $postFactory,
        CollectionFactory $collectionFactory,
        PostResource $postResource
    ) {
        $this->collectionProcessor = $collectionProcessor;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->postFactory = $postFactory;
        $this->collectionFactory = $collectionFactory;
        $this->postResource = $postResource;
    }

    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria = null
    ) {
        /** @var PostSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();

        /** @var Collection $collection */
        $collection = $this->collectionFactory->create();

        if ($searchCriteria) {
            $this->collectionProcessor->process($searchCriteria, $collection);
            $searchResults->setSearchCriteria($searchCriteria);
        }

        $searchResults
            ->setTotalCount($collection->getSize())
            ->setItems(array_values($collection->getItems()));

        return $searchResults;
    }

    /**
     * @inheritDoc
     */
    public function get($postId)
    {
        /** @var PostInterface $post */
        $post = $this->postFactory->create();

        $this->postResource->load($post, $postId);

        if (!$post->getId()) {
            throw new NoSuchEntityException(__('No post found for id %1', $postId));
        }

        return $post;
    }


    /**
     * @inheritDoc
     */
    public function getById($postId)
    {
        /** @var PostInterface $post */
        $post = $this->postFactory->create();

        $this->postResource->load($post, $postId);

        if (!$postId->getId()) {
            throw new NoSuchEntityException(__('No blog found for id %1', $postId));
        }

        return $post;
    }

    /**
     * @inheritDoc
     */
    public function save($post)
    {
        try {
            $this->postResource->save($post);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        }

        return $post;
    }

    /**
     * @inheritDoc
     */
    public function delete($post)
    {
        try {
            $this->postResource->delete($post);
        } catch (\Exception $e) {
            throw new CouldNotDeleteException(__($e->getMessage()));
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById($postId)
    {
        /** @var PostInterface $post */
        $post = $this->get($postId);

        return $this->delete($post);
    }
}