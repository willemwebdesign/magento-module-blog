<?php
/**
 * NOTICE OF LICENSE
 * This source file is subject to the General Public License (GPL 3.0).
 * This license is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/gpl-3.0.en.php
 *
 * DISCLAIMER
 * Do not edit or add to this file if you wish to upgrade this module to newer
 * versions in the future.
 * @category    Dockie: Blog package
 * @package     Dockie: Blog
 * @author        Willem Oostelbos <willemoostelbos@gmail.com>
 * @copyright   Copyright (c) 2019 Willem Oostelbos (https://www.willemoostelbos.nl)
 * @license        http://opensource.org/licenses/gpl-3.0.en.php General Public
 * License (GPL 3.0)
 */

namespace Dockie\Blog\Controller\Adminhtml\Post;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{
    protected $resultPageFactory = false;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend((__('Blog Posts')));

        return $resultPage;
    }
}
