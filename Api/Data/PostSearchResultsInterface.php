<?php
/**
 * NOTICE OF LICENSE
 * This source file is subject to the General Public License (GPL 3.0).
 * This license is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/gpl-3.0.en.php
 *
 * DISCLAIMER
 * Do not edit or add to this file if you wish to upgrade this module to newer
 * versions in the future.
 * @category    Dockie: Blog package
 * @package     Dockie: Blog
 * @author        Willem Oostelbos <willemoostelbos@gmail.com>
 * @copyright   Copyright (c) 2019 Willem Oostelbos (https://www.willemoostelbos.nl)
 * @license        http://opensource.org/licenses/gpl-3.0.en.php General Public
 * License (GPL 3.0)
 */

namespace Dockie\Blog\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface PostSearchResultsInterface extends SearchResultsInterface
{
    /**
     * @return \Dockie\Blog\Api\Data\PostInterface[]
     */
    public function getItems();

    /**
     * @param \Dockie\Blog\Api\Data\PostInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}