<?php
/**
 * NOTICE OF LICENSE
 * This source file is subject to the General Public License (GPL 3.0).
 * This license is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/gpl-3.0.en.php
 *
 * DISCLAIMER
 * Do not edit or add to this file if you wish to upgrade this module to newer
 * versions in the future.
 * @category    Dockie: Blog package
 * @package     Dockie: Blog
 * @author        Willem Oostelbos <willemoostelbos@gmail.com>
 * @copyright   Copyright (c) 2019 Willem Oostelbos (https://www.willemoostelbos.nl)
 * @license        http://opensource.org/licenses/gpl-3.0.en.php General Public
 * License (GPL 3.0)
 */

namespace Dockie\Blog\Api\Data;

interface PostInterface
{
    const TABLE = 'dockie_blog';

    const KEY_POST_ID = 'post_id';
    const KEY_TITLE = 'title';
    const KEY_ALIAS = 'alias';
    const KEY_STATUS = 'status';
    const KEY_CREATED_AT = 'created_at';
    const KEY_UPDATED_AT = 'updated_at';
    const KEY_SHORT_DESCRIPTION = 'short_description';
    const KEY_DESCRIPTION = 'description';

    /**
     * @return int
     */
    public function getId();

    /**
     * @param $id
     * @return int
     */
    public function setId($id);

    /**
     * @return int
     */
    public function getPostId();

    /**
     * @param $postId
     * @return int
     */
    public function setPostId($postId);

    /**
     * @return sting
     */
    public function getTitle();

    /**
     * @param $title
     * @return string
     */
    public function setTitle($title);

    /**
     * @return string
     */
    public function getAlias();

    /**
     * @param $alias
     * @return string
     */
    public function setAlias($alias);

    /**
     * @return boolean
     */
    public function getStatus();

    /**
     * @param $status
     * @return boolean
     */
    public function setStatus($status);

    /**
     * @return string
     */
    public function getCreatedAt();

    /**
     * @param $createdAt
     * @return string
     */
    public function setCreatedAt($createdAt);

    /**
     * @return string
     */
    public function getUpdatedAt();

    /**
     * @param $updatedAt
     * @return string
     */
    public function setUpdatedAt($updatedAt);

    /**
     * @return string
     */
    public function getShortDescription();

    /**
     * @param $shortDescription
     * @return string
     */
    public function setShortDescription($shortDescription);

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @param $description
     * @return string
     */
    public function setDescription($description);
}