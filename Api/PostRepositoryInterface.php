<?php
/**
 * NOTICE OF LICENSE
 * This source file is subject to the General Public License (GPL 3.0).
 * This license is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/gpl-3.0.en.php
 *
 * DISCLAIMER
 * Do not edit or add to this file if you wish to upgrade this module to newer
 * versions in the future.
 * @category    Dockie: Blog package
 * @package     Dockie: Blog
 * @author        Willem Oostelbos <willemoostelbos@gmail.com>
 * @copyright   Copyright (c) 2019 Willem Oostelbos (https://www.willemoostelbos.nl)
 * @license        http://opensource.org/licenses/gpl-3.0.en.php General Public
 * License (GPL 3.0)
 */

namespace Dockie\Blog\Api;

interface PostRepositoryInterface
{

    /**
     * @param int $postId
     * @return \Dockie\Blog\Api\Data\PostInterface
     * @throws NoSuchEntityException
     */
    public function getById($postId);

    /**
     * @param \Dockie\Blog\Api\Data\PostInterface $post
     * @return \Dockie\Blog\Api\Data\PostInterface
     * @throws CouldNotSaveException
     */
    public function save($post);

    /**
     * @param \Dockie\Blog\Api\Data\PostInterface $post
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete($post);

    /**
     * @param int $post
     * @return bool
     * @throws NoSuchEntityException
     * @throws CouldNotDeleteException
     */
    public function deleteById($post);

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface|null $searchCriteria
     * @return \Dockie\Blog\Api\Data\PostSearchResultsInterface
     */

    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria = null);
}